#!/bin/bash
function aptNodeInstallation(){
    # install node 
    sudo apt install nodejs -y
    # install npm 
    sudo apt install npm -y
    # install serve 
    curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
    sudo apt-get install -y nodejs

   curl -fsSL https://deb.nodesource.com/setup_21.x | sudo -E bash - &&\
   sudo apt-get install -y nodejs


   # remove the previous node version 
   sudo apt --purge remove nodejs 
   sudo apt autoremove 

   systemctl start name.service
   systemctl enable name.service 

}

function nodeInstallation(){
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

    #nvm uninstall 14.17.0
    #rm -rf "$NVM_DIR"

}

function nodeUninstallation(){
    # Uninstall all installed versions of Node.js managed by nvm
    echo "Uninstalling all installed Node.js versions..."
    nvm ls --no-colors | grep -vE '(->|alias|v[0-9]+\.[0-9]+\.[0-9]+)' | xargs -L 1 nvm uninstall

    # Remove nvm directory
    echo "Removing nvm directory..."
    rm -rf "$NVM_DIR"

    # Clean up nvm initialization lines from shell profile files
    echo "Cleaning up shell profile files..."
    for PROFILE_FILE in "$HOME/.bashrc" "$HOME/.profile" "$HOME/.zshrc" "$HOME/.bash_profile"
    do
    if [ -f "$PROFILE_FILE" ]; then
        echo "Cleaning $PROFILE_FILE..."
        sed -i '/nvm.sh/d' "$PROFILE_FILE"
        sed -i '/NVM_DIR/d' "$PROFILE_FILE"
    fi
    done

    echo "nvm and all Node.js versions uninstalled. Please restart your terminal."
}

nodeUninstallation

# sudo apt purge nodejs
# sudo apt autoremove