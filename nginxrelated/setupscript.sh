#!/bin/bash 
# > /dev/null 2>&1
function utils(){
    # to ensure that you allow the nginx to listent to the incoming request!
    sudo ufw enable 
    sudo ufw allow 22 80 443
    sudo ufw reload 
    sudo ufw status
}

function nginxInstallation(){
    echo "Updating our package index ...."
    sudo apt update > /dev/null 2>&1  && sudo apt upgrade -y > /dev/null 2>&1
    echo "Installing nginx web server..... "
    sudo apt install nginx -y
    echo "Start the nginx service ......" 
    sudo systemctl start nginx 
    # sudo sysetmctl enable  nginx 
    sudo systemctl status nginx

}

function nginxUninstallation(){
    echo "Uninstall nginx........"
    sudo systemctl stop nginx 
    sudo apt purge nginx 
    sudo apt autoremove -y
    
}

nginxInstallation