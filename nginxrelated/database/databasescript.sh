#!/bin/bash

function databaseUtilCommands(){
    getent group # to find if there is a group for the postgres
    getent passwd # to find if there is  a postgres created or not 


     # run any sql command for the postgres ,  we have switch to user postgres 
     sudo -u postgres psql

     # Run this commmand in order to change the postgres's password 
     #ALTER USER postgres WITH PASSWORD 'newpassword';


     # /etc/postgres/main/14/postgresql.conf
     # listen_addresses = '0.0.0.0'

     # /etc/post...../pg_hba.conf
     # host all all 0.0.0.0/0 md5

     sudo systemctl daemon-reload 
     sudo systemctl restart postgresql

}

function installingPostgresql(){
    sudo apt update && sudo apt upgrade -y
    sudo apt install postgresql postgresql-contrib -y
    # sudo apt install postgresql-16


    # for uninstalling the postgresql 
    sudo apt --purge remove postgresql postgres-14 postgresql-contrib -y


}

