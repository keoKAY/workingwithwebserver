#!/bin/bash

function installCertbot(){
    sudo apt update && sudo apt upgrade -y 
    sudo apt install certbot  -y 
    sudo apt install python3-certbot-nginx -y
    
    #   adding https for the service using cerbot
    sudo certbot --nginx -d reactjs.anuznomii.lol


}