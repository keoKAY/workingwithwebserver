#!/bin/bash

domainName=$1
servicePort=$2
nginxConfigDir="/etc/nginx/conf.d"

# check if domainName and servicePort are provided 
if [ -z "$domainName" ] || [ -z "$servicePort" ]; then
 echo "Domain name and service port required!! "
 exit 1 
fi


cat <<EOF | sudo tee "${nginxConfigDir}/${domainName}.conf" > /dev/null
server {
    listen 80;
    listen [::]:80;
    server_name $domainName;
    location / {
        proxy_pass http://localhost:${servicePort};
        proxy_set_header Host \$http_host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
    }
}
EOF

# check if there is any syntax error 
sudo nginx -t # test the nginx configuration file 
sudo nginx -s reload 


# echo "Adding https for the $domainName"
sudo certbot --nginx -d $domainName
echo "Successfully completed the task .....!"




